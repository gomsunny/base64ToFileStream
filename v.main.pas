unit v.main;

interface

uses
  mvw.vForm,

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Grids, Vcl.ValEdit, Vcl.ComCtrls, RzButton, RzRadChk;

type
  TvMain = class(TvForm)
    SaveDialog: TSaveDialog;
    Progress: TProgressBar;
    Label1: TLabel;
    LabelFileName: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FDst, FSrc: string;
    FInfos: TStringList;
    FBuf: TBytesStream;
    procedure Parse(const AProgressProc: TProc<Boolean, Integer>);
  protected
    procedure DoDropFile(const ACnt: Integer; const AFiles: TStringList); override;
  public
  end;

var
  vMain: TvMain;

implementation

{$R *.dfm}

uses
  Clipbrd, System.NetEncoding, System.Threading, IOUtils, mIOUtils, mUtils.Windows, UITypes
  ;

const
  SContentTypeMultipartMixed = 'Content-Type: multipart/mixed; ';
  SContentDispositionAttachment = 'Content-Disposition: attachment; ';
  SCrLf = #13#10;

procedure TvMain.DoDropFile(const ACnt: Integer; const AFiles: TStringList);
begin
  inherited;

  if ACnt > 0 then
  begin
    FBuf.Clear;
    FInfos.Clear;
    FSrc := AFiles[0];
    LabelFileName.Caption := FSrc;
    TTask.Run(procedure
      begin
        Parse(procedure(ATotal: Boolean; APos: Integer)
          begin
            if ATotal then
            begin
              Progress.Max := APos;
              Progress.Position := 0;
            end
            else
              Progress.Position := APos;
          end);
        TThread.Synchronize(nil, procedure
          begin
            SaveDialog.InitialDir := TDirectory.GetParent(FSrc);
            SaveDialog.FileName := FInfos.ValueFromIndex[1];
            FDst := TPath.Combine(SaveDialog.InitialDir, FInfos.ValueFromIndex[1]);
            FDst := TPath.MakeUniqueFileName(FDst);
            FBuf.SaveToFile(FDst);
            if (MessageDlg('작업이 완료되었습니다. 탐색기로 파일위치를 열 수 있습니다.', mtConfirmation, [mbOK, mbCancel], 0) = mrOk) then
              if TFile.Exists(FDst) then
                OpenFolderAndSelectFile(FDst);
          end);
      end);
  end;
end;

procedure TvMain.FormCreate(Sender: TObject);
begin
  EnableDropdownFiles := True;

  FBuf := TBytesStream.Create;
  FInfos := TStringList.Create;
end;

procedure TvMain.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FInfos);
  FreeAndNil(FBuf);
end;

procedure TvMain.Parse(const AProgressProc: TProc<Boolean, Integer>);
var
  LReader: TStreamReader;
  LFind, LBuf: string;
  LFileBuf: TBytes;
  LPos: Integer;
  procedure DoProgress(ATotal: Boolean; APos: Integer);
  var
    LCurrent: Integer;
  begin
    LCurrent := LReader.BaseStream.Position * 100 div LReader.BaseStream.Size;
    if ATotal then
      LPos := 0
    else if (LCurrent > LPos) then
    begin
      LPos := LCurrent;
      DoProgress(False, LPos);
      TThread.Synchronize(nil, procedure begin AProgressProc(ATotal, LPos); end);
    end;
  end;
  function MoveUntil(const ATarget: String): Boolean;
  var
    i, LRead: Integer;
  begin
    i := 0;
    repeat
      LRead := LReader.Read;
      if LRead = Integer(ATarget.Chars[i]) then
        Inc(i)
      else
        i := 0;

      Result := i = ATarget.Length;
      if Result then
      begin
        DoProgress(False, LReader.BaseStream.Position);
        Exit(True);
      end;
    until LRead = -1;
  end;
  function ReadUntil(const ATarget: String; out ARead: String): Boolean;
  var
    LRead, i: Integer;
  begin
    i := 0;
    ARead := '';
    repeat
      LRead := LReader.Read;
      ARead := ARead + Char(LRead);
      if LRead = Integer(ATarget.Chars[i]) then
        Inc(i)
      else
        i := 0;
      Result := i = ATarget.Length;
      if Result then
      begin
        ARead := ARead.Substring(0, ARead.Length - ATarget.Length);
        DoProgress(False, LReader.BaseStream.Position);
        Exit(True);
      end;
    until (LRead = -1);
  end;
begin
  LReader := TStreamReader.Create(FSrc);
  try
    DoProgress(True, 100);
    if not MoveUntil(SContentTypeMultipartMixed) then
      Exit;

    if not ReadUntil(SCrLf, LBuf) then
      Exit;

    FInfos.Add(LBuf);

    if not MoveUntil(SContentDispositionAttachment) then
      Exit;

    if not ReadUntil(SCrLf, LBuf) then
      Exit;
    FInfos.Add(LBuf.Replace('"', '', [rfReplaceAll]));

    LReader.ReadLine;
    LReader.ReadLine;
    LReader.ReadLine;
    LFind := '--' + FInfos.ValueFromIndex[0] + '--';
    while ReadUntil(SCrLF, LBuf) do
      if LBuf <> LFind then
      begin
        LFileBuf := TNetEncoding.Base64.DecodeStringToBytes(LBuf);
        FBuf.WriteData(LFileBuf, Length(LFileBuf));
      end;
    DoProgress(False, LReader.BaseStream.Position);
  finally
    LReader.Free;
  end;
end;

end.
